%global _empty_manifest_terminate_build 0
Name:		python-XStatic
Version:	1.0.3
Release:	1
Summary:	XStatic base package with minimal support code
License:	MIT
URL:		https://github.com/xstatic-py/xstatic
Source0:	https://files.pythonhosted.org/packages/d6/dd/e448da90f0e72a30546cda9b193893ea2907e7d3a702655c2c3b80c3b97f/XStatic-1.0.3.tar.gz
BuildArch:	noarch

%description
The goal of XStatic family of packages is to provide static file packages
with minimal overhead - without selling you some dependencies you don't want.
XStatic has some minimal support code for working with the XStatic-* packages.
Docs: https://xstatic.readthedocs.io/
Repository: https://github.com/xstatic-py/xstatic (base package)
Find more stuff already packaged as xstatic package there:
https://github.com/xstatic-py/
https://pypi.org/search/?q=xstatic
Licenses:
* MIT license (for XStatic code)
* same license as packaged file (for static file packages)

%package -n python3-XStatic
Summary:	XStatic base package with minimal support code
Provides:	python-XStatic = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-XStatic
The goal of XStatic family of packages is to provide static file packages
with minimal overhead - without selling you some dependencies you don't want.
XStatic has some minimal support code for working with the XStatic-* packages.
Docs: https://xstatic.readthedocs.io/
Repository: https://github.com/xstatic-py/xstatic (base package)
Find more stuff already packaged as xstatic package there:
https://github.com/xstatic-py/
https://pypi.org/search/?q=xstatic
Licenses:
* MIT license (for XStatic code)
* same license as packaged file (for static file packages)

%package help
Summary:	Development documents and examples for XStatic
Provides:	python3-XStatic-doc
%description help
The goal of XStatic family of packages is to provide static file packages
with minimal overhead - without selling you some dependencies you don't want.
XStatic has some minimal support code for working with the XStatic-* packages.
Docs: https://xstatic.readthedocs.io/
Repository: https://github.com/xstatic-py/xstatic (base package)
Find more stuff already packaged as xstatic package there:
https://github.com/xstatic-py/
https://pypi.org/search/?q=xstatic
Licenses:
* MIT license (for XStatic code)
* same license as packaged file (for static file packages)

%prep
%autosetup -n XStatic-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.rst ]; then cp -af README.rst %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.md ]; then cp -af README.md %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.txt ]; then cp -af README.txt %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-XStatic -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Jun 01 2023 wangjunqi <wangjunqi@kylinos.cn> - 1.0.3-1
- Update package to version 1.0.3

* Thu Feb 17 2022 caodongxia <caodongxia@huawei.com> - 1.0.2-2
- Add newline in nspkg.pth to avoid overrunning the line buffer

* Fri Jan 29 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
